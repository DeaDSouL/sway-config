#!/bin/bash
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 foldmethod=marker
# ----------------------------------------------------------------------


# Checks

# Make sure we can load config.sh # {{{
BASE_PATH=$(dirname $(dirname $(realpath "$0")))
CONFIG="${BASE_PATH}/install/config.sh"
if [ -f $CONFIG ]; then
    . $CONFIG
else
    echo 'Could not import config.sh'
    echo 'Aborting..'
    exit 1
fi
# }}}

# Make sure we don't have root permissions # {{{
if [ $UID -eq 0 ]; then
    echo 'This script must be executed as a normal user.'
    echo 'Aborting..'
    exit 1
fi
# }}}

# Make sure we are on Fedora # {{{
OSRELEASE=/etc/os-release
DISTRO=$(grep ^ID= $OSRELEASE 2>/dev/null | cut -d'=' -f2 2>/dev/null)
if [[ ! -f $OSRELEASE || $DISTRO != 'fedora' ]]; then
    echo 'This distro is not supported'
    echo 'The installer was written specifically for Fedora.'
    echo 'Aborting..'
    exit 1
fi
# }}}


# Setting things up

# Setting up dirs & files # {{{
[ -f "${TMPDIR}" ] && mv -Z "${TMPDIR}" "${TMPDIR}.bkp-${DTIME}" >/dev/null 2>&1
[ ! -e "${TMPDIR}" ] && mkdir -Zp "${TMPDIR}" >/dev/null 2>&1

for f in ${ENV} ${STDOUT} ${STDERR} ${INRPMS} ${INPIPS} ${DNFREPOS} ${FLATPAKREPOS}; do
    [ -d "${f}" ] && mv -Z "${f}" "${f}.bkp-${DTIME}" >/dev/null 2>&1
    [ ! -f "${f}" ] && touch "${f}" >/dev/null 2>&1
done
# }}}

# Storing needed variables & values # {{{
# --------------------
echo "USERNAME=$USERNAME" > "${VARS}"
echo "DISPLAY=$DISPLAY" >> "${VARS}"
echo "SWAYSOCK=$SWAYSOCK" >> "${VARS}"
echo "XDG_RUNTIME_DIR=$XDG_RUNTIME_DIR" >> "${VARS}"
echo "DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS" >> "${VARS}"
echo "WAYLAND_DISPLAY=$WAYLAND_DISPLAY" >> "${VARS}"
# }}}

# Header Message # {{{
# --------------------
echo -e "\nTo see output: ${STDOUT}"
echo -e "To see errors: ${STDERR}\n"
# }}}

su -c "env RUN_ASROOT=true bash ${BASE_PATH}/install/asroot.sh" && swaymsg reload >>"${STDOUT}" 2>>"${STDERR}"

