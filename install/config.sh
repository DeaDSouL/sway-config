#!/bin/bash
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 foldmethod=marker
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
#         S T A R T    O F    C O N F I G U R A T I O N
# ----------------------------------------------------------------------

# Specify the tmp dir location and name
TMPDIR='/tmp/ds.sway'

# Settings: 'yes' OR 'no' # {{{
# -----------------------

# Info {{{
# Copy the dotfiles configs:
#   - dunst
#   - imv
#   - rofi
#   - sway
#   - waybar
#   - swaylock
#   - zathura
#   - Thunar
# }}}
COPY_DOTFILES='yes'

# Info {{{
# Copy the appearance configs:
#   - SDDM themes
#   - Icons
#   - GTK themes (GNOME)
#   - QT themes (KDE)
# }}}
COPY_APPEARANCE='yes'

# Info {{{
# Should we automatically configure available monitors and
# set their best/highest resolution?
# }}}
CONFIGURE_MONITORS='yes'

# Info {{{
# Copy the wallpapers in the cloned Swayora repository
# to ~/Pictures/wallpapers/deadsoul
# }}}
COPY_WALLPAPER='yes'

# Info {{{
# Tweak DNF to be faster
#   add:
#       fastestmirror=true
#       max_parallel_downloads=10
# }}}
TWEAK_DNF='yes'

# Add RPMFusion repositories
ADD_RPMFUSION='yes'

# Info {{{
# Add Brave browser repository
# And install it
# }}}
ADD_BRAVE='no'

# Info {{{
# Install the Flatpak package
# And add the Flathub repository
# }}}
ADD_FLATPAK='yes'

# Info {{{
# Install the packages mentioned in the variable: $_PKGS_INSTALL
# Scroll down to see and modify them
# }}}
ADD_SELECTED_PKGS='yes'

# Info {{{
# Install the required packages for vimConfig, which are:
#   vim-enhanced ctags cmake gcc-c++ make yarnpkg nodejs
#   python3-flake8 python3-devel mono-complete golang npm
#   java-17-openjdk-devel java-17-openjdk-headless
# }}}
ADD_VIMCONFIG_PKGS='yes'

# Info {{{
# Clone DeaDSouL's vimConfig
# https://gitlab.com/DeaDSouL/vimConfig
# Once it got cloned, and all its requirements were installed,
# you may want to do the following:
#   1. Run vim
#   2. Upgrade vim-plug, by hitting: ,U
#   3. Install/Update plugins, by hitting: ,u
#   4. Exit vim, then install the fonts:
#       $HOME/.vim/plugged/fonts/./install.sh
# }}}
ADD_VIMCONFIG='yes'

# Info {{{
# Should we automatically configure sudoers for your username in
# order to be able to execute /home/username/.config/sway/bin/kbbl
# with sudo without being asked for a password?
# }}}
CONFIGURE_SUDOERS='yes'

# Install PIP (Required for our python scripts)
ADD_PIP='yes'

# Info {{{
# Create the required python environment for
# our python scripts in sway/bin/
# (Required for our python scripts)
#
# If you enable this, it will install python3-pip package, even
# if ADD_PIP was disabled by having a value of 'no'.
# }}}
ADD_PIPENV_ENV='yes'

# Info {{{
# Install the Nerd fonts
# Repo: https://github.com/ryanoasis/nerd-fonts.git
# }}}
ADD_NERD_FONTS='yes'

# Info {{{
# Install Noir Wallpapers
# they are a fine dark bluish grey wallpapers
# Repo: https://gitlab.com/by-others/noir-wallpapers.git
# }}}
ADD_NOIR_WALLPAPERS='yes'
# }}}

# APPEARANCE Settings: 'user' OR 'system' # {{{
# -------------------------------------

# Info {{{
# Where to install the icons
#   - For current user only, use:   user
#       location: ~/.icons
#   - Globally, for all users, use: system
#       location: /usr/share/icons
# }}}
INSTALL_APPEARANCE_ICONS_TO='system'

# Info {{{
# Where to install the GTK theme
#   - For current user only, use:   user
#       location: ~/.themes
#   - Globally, for all users, use: system
#       location: /usr/share/themes
# }}}
INSTALL_APPEARANCE_GTK_TO='system'

# Info {{{
# Where to install the QT (KDE) theme
#   - For current user only, use:   user
#       location: ~/.local/share/plasma/desktoptheme
#   - Globally, for all users, use: system
#       location: /usr/share/plasma/desktoptheme
# }}}
INSTALL_APPEARANCE_KDE_TO='system'

# SDDM theme {{{
# It will be installed in
#   - /usr/share/sddm/themes
# }}}
# }}}

# DeaDSouL Packages # {{{
# --------------------

# ----------------------------------------------------------------------
# START: Ignore the following 7 lines # {{{
if [[ $(grep VERSION_ID /etc/os-release 2>/dev/null | cut -d'=' -f2) -eq 38 ]]; then
    # == F38
    _FONTAWESOME=(fontawesome5-fonts-all fontawesome-fonts)
else
    # >= F39
    _FONTAWESOME=(fontawesome-fonts-all fontawesome4-fonts)
fi
# END: Ignore the above 7 lines # }}}
# ----------------------------------------------------------------------

_PKGS_DNFPLUGINS=( # {{{
    #dnf-plugin-system-upgrade      # got removed
    dnf-plugins-core
) # }}}
_PKGS_NVIDIA=( # {{{
    akmod-nvidia
    xorg-x11-drv-nvidia-cud
) # }}}
_PKGS_SNAPPER=( # {{{
    snapper
    python3-dnf-plugin-snapper
    pam_snapper
) # }}}
_PKGS_SYSMON=( # {{{
    htop
    btop    # bashtop old name
    glances
    conky
) # }}}
_PKGS_EDITOR=( # {{{
    vim-enhanced
    kate
) # }}}
_PKGS_FONTS=( # {{{
    #ubuntu-title-fonts     # got removed
    ${_FONTAWESOME[@]}
) # }}}
_PKGS_GRAPHICS=( # {{{
    #freecad gimp-heif-plugin   # got removed f39: 
    digikam
    gimp
    darktable
    rawtherapee
    darktable-tools-noise
    libwebp-tools
    imv
) # }}}
_PKGS_VIDEO=( # {{{
    fswebcam
    kdenlive
    vlc
    vlc-extras
    vlc-bittorrent
    mpv
) # }}}
_PKGS_DEV=( # {{{
    git
    python3-pip
    sqlitebrowser
    sassc
) # }}}
_PKGS_DEVLIBS=( # {{{
    python3-devel
) # }}}
_PKGS_LIBS=( # {{{
    ffmpeg
    x264
    gstreamer1-plugins-bad-free
    gstreamer1-plugins-bad-free-extras
    gstreamer1-plugin-openh264
) # }}}
_PKGS_PLUGINS=( # {{{
    dolphin-plugins
    ffmpegthumbs
) # }}}
_PKGS_SECURITY=( # {{{
    kleopatra
    lynis
    chkrootkit
    rkhunter
    unhide
) # }}}
_PKGS_FILESYSTEM=( # {{{
    dmg2img
    exfatprogs
    zfs-fuse
    #fuse-exfat     # got removed
    #exfat-utils    # got removed
    gnome-disk-utility
) # }}}
_PKGS_NET=( # {{{
    akmod-wl
    iftop
    nethogs
    filezilla
    dropbox
    axel
) # }}}
_PKGS_SYS=( # {{{
    cockpit
    lm_sensors
    kcm_systemd
) # }}}
_PKGS_GUI=( # {{{
    latte-dock
    yakuake
) # }}}
_PKGS_CLI=( # {{{
    powerline
    tmux
    tmux-powerline
    ranger
    unrar
    xdotool
    xbindkeys
    fzf
) # }}}
_PKGS_SOCIAL=( # {{{
    thunderbird
    irssi
    perl-CPAN
    perl-LWP-Protocol-https
) # }}}
_PKGS_MISC=()

# Info {{{
# Add whatever you want from the above as groups, or packages..
# or even add yours to the following array
# ex:
#   if you want to install all packages that are mentioned in $_PKGS_GRAPHICS, use this:
#       ${_PKGS_GRAPHICS[@]}
#   If you want to install only digikam and gimp, use this:
#       digikam gimp
# }}}
_PKGS_INSTALL=(
    ${_PKGS_DNFPLUGINS[@]}
    ${_PKGS_SNAPPER[@]}
    ${_PKGS_SYSMON[@]}
    ${_PKGS_EDITOR[@]}
    ${_PKGS_FONTS[@]}
    ${_PKGS_GRAPHICS[@]}
    ${_PKGS_DEV[@]}
    ${_PKGS_DEVLIBS[@]}
    ${_PKGS_LIBS[@]}
    ${_PKGS_SECURITY[@]}
    ${_PKGS_FILESYSTEM[@]}
    ${_PKGS_NET[@]}
    ${_PKGS_SYS[@]}
    ${_PKGS_SOCIAL[@]}
    tmux
    ranger
    powerline
    tmux-powerline
    unrar
    xbindkeys
    fzf
    fswebcam
    vlc
    vlc-extras
    vlc-bittorrent
    mpv
    imv
)
# }}}

# ----------------------------------------------------------------------
#           E N D    O F    C O N F I G U R A T I O N
# ----------------------------------------------------------------------


# Variables # {{{
# --------------------
USERNAME=$(id -un)

BASE_PATH=$(dirname $(dirname $(realpath "$0")))

OSESCHE='\e'                    # Escape character‌​ for 'echo' in colors
NC=${OSESCHE}'[0m'              # No Color: Text Reset
RED=${OSESCHE}'[0;31m'          # Red
GREEN=${OSESCHE}'[0;32m'        # Green
BLUE=${OSESCHE}'[0;34m'         # Blue
BLACK=${OSESCHE}'[0;30m'        # Black
WHITE=${OSESCHE}'[0;37m'        # White
BWHITE=${OSESCHE}'[1;37m'       # Bold White
FG=${WHITE}

PREVLINE='\e[1A'
CLRLINE='\e[K'

DONEEMPTY='[    ]'
DONEEMPTY="${BLUE}[${NC}${FG}    ${BLUE}]${NC}${FG}"
DONEFILLED='[Done]'
DONEFILLED="${BLUE}[${NC}${GREEN}Done${BLUE}]${NC}${FG}"
FAILFILLED='[Fail]'
FAILFILLED="${BLUE}[${NC}${RED}Fail${BLUE}]${NC}${FG}"

DTIME_HUMAN=$(date +'%Y/%m/%d %H:%M:%S')
DTIME=$(date -d "$DTIME_HUMAN" +'%Y%m%d_%H%M%S')

CPDIRS=(dunst imv rofi sway swayora waybar swaylock zathura Thunar)

VARS="${TMPDIR}/vars"
STDOUT="${TMPDIR}/log-out"
STDERR="${TMPDIR}/log-err"
INRPMS="${TMPDIR}/installed_rpms"
INPIPS="${TMPDIR}/installed_pips"
DNFREPOS="${TMPDIR}/dnf_repos"
FLATPAKREPOS="${TMPDIR}/flatpak_repos"
# }}}

