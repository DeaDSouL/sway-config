#!/bin/bash
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 foldmethod=marker
# ----------------------------------------------------------------------
# @TODO: # {{{
#       - snapper ??
#       - do dnf upgrade --refresh, before running the script?
#           - (what if there's a kernel upgrade??)
#           - it's better to ask user to upgrade everything, before running the script
#       - if foot 'Installer logs output' is running, do NOT re-run it
#       - what if the user didn't select any package to install ??
#       - re-set the default packages
#       - adding the ability of selecting flatpaks to install from them?
#   DONE
#       - do check for the existence of .vimrc and .vim
#       - do check why vimrc is the old version
#       - do check for existence of nerd-fonts
#       - open foot and show the stdout
#       - add default wallpaper
#       - make sure install.sh does not get executed with root powers
#       - re-enable re-building rpm db
#       - once the script is finished, add notes about vim
#       - fix awesomefont different packages in f38 & f39
#       - since we're using add requirements alot, it's better to make a function for that
#       - test the new install_rpms() in VM
#       - run pick-wallpaper.sh to generate the cache once the installation is done.
#       - run random_bg.py daemon once the installation is done.
#       - check/fix why doesn't `random_bg.sh start` run the daemon once the
#         installation is done. Done, it was because of wayland security.
#       - fix the re-creation of ~/.config/wallpaper everytime we run the installer.
#       - check tmp log in random_bg.*
#       - resolve the new dotfiles path
#       - use our `kbbl` only if `light` is missing
# }}}
# ----------------------------------------------------------------------


# Checks

# Make sure we were being executed by install.sh # {{{
if [ -z "$RUN_ASROOT" ]; then
    echo "Error: This script can not be executed directly."
    echo 'Aborting..'
    exit 1
fi
# }}}

# Make sure we have root privileges # {{{
if [ $UID -ne 0 ]; then
    echo 'This script must be executed as root or with sudo.'
    echo 'Aborting..'
    exit 1
fi
# }}}

# Make sure we can load config.sh # {{{
BASE_PATH=$(dirname $(dirname $(realpath "$0")))
CONFIG="${BASE_PATH}/install/config.sh"
if [ -f "$CONFIG" ]; then
    . "$CONFIG"
else
    echo 'Could not import config.sh'
    echo 'Aborting..'
    exit 1
fi
# }}}

# Make sure we can load vars # {{{
if [ -f "$VARS" ]; then
    . "$VARS"
else
    echo 'Could not import the generated vars'
    echo 'Aborting..'
    exit 1
fi
# }}}

# Make sure $USERNAME has an existing user # {{{
if [[ -z "$USERNAME" || "$USERNAME" == 'root' || $(id "${USERNAME}" >/dev/null 2>&1; echo $?) -gt 0 ]]; then
    if [ -z "$USERNAME" ]; then
        echo "Missing username!"
    elif [ "$USERNAME" == 'root' ]; then
        echo "Can not install the script for '${USERNAME}'!"
    else
        echo "The username '${USERNAME}' does not exist!"
    fi
    echo 'Aborting..'
    exit 1
fi
UHOME="/home/${USERNAME}"
# }}}

# Functions # {{{
# --------------------
function get_missing_rpms() {
    QPKGS=$@
    MISSING=''
    for pkg in ${QPKGS[@]}; do
        if ! grep ^${pkg}$ ${INRPMS} >/dev/null 2>&1; then
            MISSING="${MISSING}${pkg} "
        fi
    done
    echo $MISSING
}

function install_rpms() {
    # $1    : string    headline
    # $2    : optional  --allowerasing
    # $2/$3 : array     packages
    headline="$1"
    shift 1

    allow_erasing_flag=""
    if [ "$1" == "--allowerasing" ]; then
        allow_erasing_flag="--allowerasing"
        shift 1
    fi

    echo -e "\t${DONEEMPTY} ${headline}."
    missing=$(get_missing_rpms "$*")
    if [ ! -z "${missing}" ]; then
        dnf -y install ${allow_erasing_flag} ${missing} >>"${STDOUT}" 2>>"${STDERR}"
        # Re-Building installed RPM database
        rpm -qa --queryformat='%{NAME}\n' > "${INRPMS}" 2>>"${STDERR}"
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} ${headline}."
    else
        if [ $# -gt 1 ]; then
            echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} ${headline}. (Already were installed)"
        else
            echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} ${headline}. (Already was installed)"
        fi
    fi
}

function auto_rename() {
    mv -Zv "$1"{,.bkp-${DTIME}} >>"${STDOUT}" 2>>"${STDERR}"
}

function rename_if_file() {
    [ -f "$1" ] && auto_rename "$1"
}

function rename_if_dir() {
    [ -d "$1" ] && auto_rename "$1"
}

function rename_if_link() {
    [ -L "$1" ] && auto_rename "$1"
}

function rename_if_exist() {
    [ -e "$1" ] && auto_rename "$1"
}

function do_as_user() {
    su - ${USERNAME} -c "env \
        DISPLAY=$DISPLAY \
        SWAYSOCK=$SWAYSOCK \
        XDG_RUNTIME_DIR=$XDG_RUNTIME_DIR \
        DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS \
        WAYLAND_DISPLAY=$WAYLAND_DISPLAY \
        bash -c '$1' >>'${STDOUT}' 2>>'${STDERR}'" >>"${STDOUT}" 2>>"${STDERR}"
}

function do_as_user_noredirect() {
    su - ${USERNAME} -c "env \
        DISPLAY=$DISPLAY \
        SWAYSOCK=$SWAYSOCK \
        XDG_RUNTIME_DIR=$XDG_RUNTIME_DIR \
        DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS \
        WAYLAND_DISPLAY=$WAYLAND_DISPLAY \
        bash -c '$1'" >>"${STDOUT}" 2>>"${STDERR}"
}

function show_stdout() {
    su - ${USERNAME} -c "env \
        DISPLAY=$DISPLAY \
        SWAYSOCK=$SWAYSOCK \
        XDG_RUNTIME_DIR=$XDG_RUNTIME_DIR \
        DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION_BUS_ADDRESS \
        WAYLAND_DISPLAY=$WAYLAND_DISPLAY \
        /usr/bin/foot --title='Installer output log..' bash -c 'tail -f ${STDOUT} 2>/dev/null' \
        >>'${STDOUT}' 2>>'${STDERR}'" >>"${STDOUT}" 2>>"${STDERR}"
}

function get_directories_in() {
    [ ! -d "$1" ] && return 1
    find "$1" -maxdepth 1 -mindepth 1 -type d -exec basename '{}' \;
    return 0
}
# }}}


echo -e "\n${BWHITE}As root:${NC}${FG}"; show_stdout &

# Building DBs (1/3) # {{{
# --------------------
echo -e "\t${DONEEMPTY} Building Databases."
rpm -qa --queryformat='%{NAME}\n' > ${INRPMS}
ls -1 /etc/yum.repos.d/ > ${DNFREPOS}
echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Building Databases."
# }}}

# Configuring DNF # {{{
# --------------------
#
# @TODO: Enhance it by checking for variables like `fastestmirror`
#        if found then change its value, if not then add it
#
if [ $TWEAK_DNF == 'yes' ]; then
    DNF_CONFIG='/etc/dnf/dnf.conf'
    echo -e "\t${DONEEMPTY} Configuring $DNF_CONFIG."

    if ! grep '# By Swayora' $DNF_CONFIG >/dev/null; then
        echo '' >> $DNF_CONFIG 2>>${STDERR}
        echo '# By Swayora' >> $DNF_CONFIG 2>>${STDERR}
        echo 'fastestmirror=True' >> $DNF_CONFIG 2>>${STDERR}
        echo 'max_parallel_downloads=10' >> $DNF_CONFIG 2>>${STDERR}
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Configuring $DNF_CONFIG."
    else
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Configuring $DNF_CONFIG. (Already was configured)"
    fi
fi
# }}}

# Adding RPMFusion # {{{
# --------------------
if [ $ADD_RPMFUSION == 'yes' ]; then
    echo -e "\t${DONEEMPTY} Adding RPMFusion Repository."
    if ! grep 'rpmfusion' ${DNFREPOS} >/dev/null 2>&1; then
        dnf -y install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm >>${STDOUT} 2>>${STDERR}
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Adding RPMFusion Repository."
    else
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Adding RPMFusion Repository. (Already was added)"
    fi
fi
# }}}

# Adding Brave # {{{
# --------------------
if [ $ADD_BRAVE == 'yes' ]; then
    echo -e "\t${DONEEMPTY} Adding Brave Browser Repository."
    if ! grep 'brave-browser' ${DNFREPOS} >/dev/null 2>&1; then
        dnf config-manager --add-repo https://brave-browser-rpm-release.s3.brave.com/brave-browser.repo >>${STDOUT} 2>>${STDERR}
        rpm --import https://brave-browser-rpm-release.s3.brave.com/brave-core.asc >>${STDOUT} 2>>${STDERR}
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Adding Brave Browser Repository."
    else
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Adding Brave Browser Repository. (Already was added)"
    fi

    pkgs=(brave-browser)
    install_rpms "Installing Brave Browser" ${pkgs[@]}
fi
# }}}

# Adding Flatpak & Flathub Repo & Building DBs (2/3) # {{{
# --------------------
if [ $ADD_FLATPAK == 'yes' ]; then
    pkgs=(flatpak)
    install_rpms "Installing Flatpak" ${pkgs[@]}

    # Building DBs (2/3)
    flatpak remotes > "${FLATPAKREPOS}" 2>>"${STDERR}"

    echo -e "\t${DONEEMPTY} Adding Flathub Repository."
    if ! grep 'flathub' ${FLATPAKREPOS} >/dev/null 2>&1; then
        flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo >>${STDOUT} 2>>${STDERR}
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Adding Flathub Repository."
    else
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Adding Flathub Repository. (Already was added)"
    fi
fi
# }}}

# Adding Swayora requirements & rebuilding installed RPM DB # {{{
# --------------------
pkgs=(jq libnotify zathura zathura-plugins-all)
install_rpms "Installing Swayora requirements" ${pkgs[@]}
# }}}

# Adding Swayora selected packages & rebuilding installed RPM DB # {{{
# --------------------
if [ $ADD_SELECTED_PKGS == 'yes' ]; then
    HEADLINE="Installing selected packages"
    if [ ${#_PKGS_INSTALL[@]} -gt 0 ]; then
        install_rpms "${HEADLINE}" --allowerasing ${_PKGS_INSTALL[@]}
    else
        echo -e "\t${DONEFILLED} ${HEADLINE}. (Nothing to install)"
    fi
fi
# }}}

# Adding vimConfig requirements & rebuilding installed RPM DB # {{{
# --------------------
if [ $ADD_VIMCONFIG_PKGS == 'yes' ]; then
    pkgs=(git vim-enhanced ctags cmake gcc-c++ make yarnpkg python3-flake8 python3-devel mono-complete golang nodejs nodejs-npm java-17-openjdk-devel java-17-openjdk-headless)
    install_rpms "Installing vimConfig requirements" ${pkgs[@]}
fi
# }}}

# Adding Nerd-Fonts requirements & rebuilding installed RPM DB # {{{
# --------------------
if [ $ADD_NERD_FONTS == 'yes' ]; then
    pkgs=(git)
    install_rpms "Installing Nerd-Fonts requirements" ${pkgs[@]}
fi
# }}}

# Adding Noir-Wallpapers requirements & rebuilding installed RPM DB # {{{
# --------------------
if [ $ADD_NOIR_WALLPAPERS == 'yes' ]; then
    pkgs=(git)
    install_rpms "Installing Noir-Wallpapers requirements" ${pkgs[@]}
fi
# }}}

# python3-pip & Building DBs (3/3) # {{{
# --------------------
if [ $ADD_PIP == 'yes' ]; then
    pkgs=(python3-pip)
    install_rpms "Installing PIP" ${pkgs[@]}
    # Building DBs (3/3)
    do_as_user_noredirect "pip list --format=freeze | cut -d'=' -f1 > '${INPIPS}' 2>>'${STDERR}'"
fi
# }}}

# Configuring /etc/sudoers # {{{
# --------------------
if [[ $CONFIGURE_SUDOERS == 'yes' && ! -f '/usr/bin/light' ]]; then
    BACKLIGHT_FILE="${UHOME}/.config/sway/bin/kbbl"
    SUDOERS_CONFIG='/etc/sudoers'
    SUDOERS_COMMENT="# Let $USERNAME be able to execute $BACKLIGHT_FILE without being asked for a password"
    echo -e "\t${DONEEMPTY} Configuring $SUDOERS_CONFIG."

    if ! grep "$SUDOERS_COMMENT" $SUDOERS_CONFIG >/dev/null 2>&1; then
        echo '' >> $SUDOERS_CONFIG 2>>${STDERR}
        echo '# By Swayora' >> $SUDOERS_CONFIG 2>>${STDERR}
        echo "$SUDOERS_COMMENT" >> $SUDOERS_CONFIG 2>>${STDERR}
        echo "${USERNAME} ALL=(ALL)  NOPASSWD: ${BACKLIGHT_FILE}" >> $SUDOERS_CONFIG 2>>${STDERR}
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Configuring $SUDOERS_CONFIG."
    else
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Configuring $SUDOERS_CONFIG. (Already was configured)"
    fi
fi
# }}}


echo -e "\n${BWHITE}As ${USERNAME}:${NC}${FG}"

# Copying dirs to ~/.config/ # {{{
# --------------------
if [ $COPY_DOTFILES == 'yes' ]; then
    echo -e "\t${DONEEMPTY} Copying config directories."
    for d in ${CPDIRS[@]}; do
        dst="${UHOME}/.config"
        dpath="${dst}/${d}"
        rename_if_exist "${dpath}"
        cp -ZPRva "${BASE_PATH}/dotfiles/${d}" "${dst}/" >>"${STDOUT}" 2>>"${STDERR}"
    done
    echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Copying config directories."
fi
# }}}

# @TODO: has not been tested yet!!
# Appearance # {{{
# --------------------
if [ $COPY_APPEARANCE == 'yes' ]; then
    function get_appearance_deploy_type() { # {{{
        # 1: appearance_type
        #       icons|sddm|themes-gtk|themes-qt
        #       ex: icons

        case $1 in
            icons)      deploy_type="${INSTALL_APPEARANCE_ICONS_TO}"    ;;
            themes-gtk) deploy_type="${INSTALL_APPEARANCE_GTK_TO}"      ;;
            themes-qt)  deploy_type="${INSTALL_APPEARANCE_KDE_TO}"      ;;
            sddm)       deploy_type="system"                            ;;
            #sddm)       deploy_type="/usr/share/sddm/themes"            ;;
            *)          return 1                                        ;;
        esac

        echo "${deploy_type}"
        return 0
    } # }}}
    function get_appearance_deploy_path() { # {{{
        # 1: appearance_type
        #       icons|sddm|themes-gtk|themes-qt
        #       ex: icons

        # user|system
        deploy_type=$(get_appearance_deploy_type "$1")

        case $1 in
            icons)
                [ $deploy_type == 'user' ] && deploy_path='~/.icons'
                deploy_path="${deploy_path:-'/usr/share/icons'}"
                ;;
            themes-gtk)
                [ $deploy_type == 'user' ] && deploy_path='~/.themes'
                deploy_path="${deploy_path:-'/usr/share/themes'}"
                ;;
            themes-qt)
                [ $deploy_type == 'user' ] && deploy_path='~/.local/share/plasma/desktoptheme'
                deploy_path="${deploy_path:-'/usr/share/plasma/desktoptheme'}"
                ;;
            sddm)       deploy_path="/usr/share/sddm/themes"            ;;
            *)          return 1                                        ;;
        esac

        echo "${deploy_path}"
        return 0
    } # }}}
    function copy_appearance() { # {{{
        # 1: appearance_type
        #       icons|sddm|themes-gtk|themes-qt
        #       ex: icons

        local \
            appearance_type="$1" \
            appearance_path="${BASE_PATH}/appearance/$1"

        [ ! -d "${appearance_path}" ] && return 1

        local \
            appearances=($(get_directories_in "${appearance_path}")) \
            deploy_type=$(get_appearance_deploy_type "${appearance_type}") \
            deploy_path=$(get_appearance_deploy_path "${appearance_type}")

        setenforce permissive
        for appearance in "${appearances[@]}"; do
            rename_if_exist "${deploy_path}/${appearance}"
            cp -ZPRva "${appearance_path}/${appearance}" "${deploy_path}/" >>"${STDOUT}" 2>>"${STDERR}"

            if [ $deploy_type == 'user' ]; then
                chown -Rv "${USERNAME}:${USERNAME}" "${deploy_path}/${appearance}" >>"${STDOUT}" 2>>"${STDERR}"
                chcon -Rv -u unconfined_u -r object_r -t user_home_t "${deploy_path}/${appearance}" >>"${STDOUT}" 2>>"${STDERR}"
            else
                chown -Rv root:root "${deploy_path}/${appearance}" >>"${STDOUT}" 2>>"${STDERR}"
                chcon -Rv -u system_u -r object_r -t user_t "${deploy_path}/${appearance}" >>"${STDOUT}" 2>>"${STDERR}"
            fi
        done
        setenforce enforcing
    } # }}}

    # icons # {{{
    echo -e "\t${DONEEMPTY} Copying Appearance: icons."
    copy_appearance icons
    echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Copying Appearance: icons."
    # }}}
    # gtk # {{{
    echo -e "\t${DONEEMPTY} Copying Appearance: GTK themes."
    copy_appearance themes-gtk
    echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Copying Appearance: GTK themes."
    # }}}
    # qt # {{{
    echo -e "\t${DONEEMPTY} Copying Appearance: QT themes."
    copy_appearance themes-qt
    echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Copying Appearance: QT themes."
    # }}}
    # sddm # {{{
    echo -e "\t${DONEEMPTY} Copying Appearance: SDDM themes."
    copy_appearance sddm
    echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Copying Appearance: SDDM themes."
    # }}}
    # user-avatar # {{{
    _face_path="${UHOME}/.face.icon"
    # copy our user-avatar only if the user doesn't currently have one
    if [ ! -e "${_face_path}" ]; then
        echo -e "\t${DONEEMPTY} Copying Appearance: User-Avatar."
        cp -ZPva "${BASE_PATH}/appearance/user-avatar" "${_face_path}" >>"${STDOUT}" 2>>"${STDERR}"
        chown -v "${USERNAME}:${USERNAME}" "${_face_path}" >>"${STDOUT}" 2>>"${STDERR}"
        setenforce permissive
        chcon -v -u unconfined_u -r object_r -t user_home_t "${_face_path}" >>"${STDOUT}" 2>>"${STDERR}"
        setenforce enforcing
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Copying Appearance: User-Avatar."
    fi
    # }}}
fi
# }}}

# Configuring monitors & thier modes # {{{
# --------------------
if [ $CONFIGURE_MONITORS == 'yes' ]; then
    echo -e "\t${DONEEMPTY} Configuring monitors and their modes."

    MONITORS_CONF="${UHOME}/.config/sway/config.d/20-monitors.conf"

    if ! grep '# Generated by Swayora' $MONITORS_CONF >/dev/null 2>&1; then
        DT=$(echo $DTIME_HUMAN | sed 's/ / at /')

        # Get sway output JSON data
        SWAYMSG_OUTPUTS=$(swaymsg --raw -t get_outputs)

        # Parse monitor names
        declare -A selected_modes[monitor_names]=$(echo "$SWAYMSG_OUTPUTS" | jq -r '.[].name')

        # Prepare 20-monitors.conf
        rename_if_exist "${MONITORS_CONF}"
        do_as_user "touch '${MONITORS_CONF}'"
        echo -e "#\n# Generated by Swayora on ${DT}\n##\n# To list monitors names & modes try: swaymsg -t get_outputs\n#\n\n" > "${MONITORS_CONF}"

        # Loop through monitor names
        for monitor_name in ${selected_modes[monitor_names]}; do
            modes=$(echo "$SWAYMSG_OUTPUTS" | jq -r --arg monitor "$monitor_name" '.[] | select(.name == $monitor).modes')
            highest_resolution=""
            highest_refresh=0
            highest_width=0
            highest_height=0

            for mode in $(echo "${modes}" | jq -c '.[]'); do
                # Extract monitor's modes
                width=$(echo "${mode}" | jq -r '.width')
                height=$(echo "${mode}" | jq -r '.height')
                refresh=$(echo "${mode}" | jq -r '.refresh' | sed 's/\(.*\)\(...\)/\1.\2/')
                #aspect_ratio=$(echo "${mode}" | jq -r '.picture_aspect_ratio')
                #resolution="${width}x${height} @ ${refresh}Hz"
                resolution="${width}x${height}@${refresh}Hz"

                # Find highest resolution
                if [[ $width -gt $highest_width || \
                    ( $width -eq $highest_width && $height -gt $highest_height ) ]]; then
                    highest_width="${width}"
                    highest_height="${height}"
                    highest_refresh="${refresh}"
                    highest_resolution="${resolution}"
                fi
            done

            # Store highest resolution for current monitor
            #selected_modes[$monitor_name]=$highest_resolution

            # Save highest resolution to our 20-monitors.conf
            echo -e "output ${monitor_name} {\n\tmode ${highest_resolution}\n\tbg ~/.config/wallpaper fill\n}\n" >> "${MONITORS_CONF}"
        done

        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Configuring monitors and their modes."
    else
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Configuring monitors and their modes. (Already was configured)"
    fi
fi
# }}}

# Copying wallpapers # {{{
# --------------------
if [ $COPY_WALLPAPER == 'yes' ]; then
    echo -e "\t${DONEEMPTY} Copying wallpapers."
    wps_dst="${UHOME}/Pictures/wallpapers"
    wps_src="${BASE_PATH}/wallpapers"
    wpdir='swayora'
    wp_default_dst="${UHOME}/.config/wallpaper"
    wp_default_src="${wps_dst}/${wpdir}/default"

    # if ~/.config/wallpaper is dir, rename it
    rename_if_dir "${wp_default_dst}"

    # if ~/.config/wallpaper is an existed file/link and it's differ than wallpapers/default
    if [[ -f "${wp_default_dst}" && $(cmp -s "${wp_default_dst}" "${wps_src}/default" >/dev/null 2>&1; echo $?) -ne 0 ]]; then
        mv -Zv ${wp_default_dst}{,.bkp-${DTIME}} >>"${STDOUT}" 2>>"${STDERR}"
    fi

    # if ~/.config/wallpaper is a link and it's differ than ~/Pictures/wallpapers/default
    # @TODO:    this condition may never occur, since the above
    #           condition handles ~/.config/wallpaper very well.
    #           we might want to delete the following condition
    if [[ -L "${wp_default_dst}" && -f "${wp_default_dst}" && "$(readlink ${wp_default_dst})" != "${wp_default_src}" ]]; then
        mv -Zv ${wp_default_dst}{,.bkp-${DTIME}} >>"${STDOUT}" 2>>"${STDERR}"
    fi

    # if ~/.config/wallpaper is not a file nor a link
    [[ ! -f "${wp_default_dst}" && ! -L "${wp_default_dst}" ]] && do_as_user "ln -sv '${wp_default_src}' '${wp_default_dst}'"

    # if ~/Pictures/wallpapers is a file, rename it
    rename_if_file "${wps_dst}"

    # if ~/Pictures/wallpapers/swayora is a file, rename it
    rename_if_file "${wps_dst}/${wpdir}"

    # if ~/Pictures/wallpapers/swayora doesn't exist, create it
    [ ! -d "${wps_dst}/${wpdir}" ] && do_as_user "mkdir -Zpv '${wps_dst}/${wpdir}'"

    for wp_src in $(find "${wps_src}" -type f -or -type l); do
        bname=$(basename ${wp_src})
        wp_dst="${wps_dst}/${wpdir}/${bname}"

        rename_if_dir "${wp_dst}"

        if [[ -f "${wp_dst}" && $(cmp -s "${wp_dst}" "${wp_src}" >/dev/null 2>&1; echo $?) -ne 0 ]]; then
            mv -Zv "${wp_dst}"{,.bkp-${DTIME}} >>${STDOUT} 2>>${STDERR}
        fi

        [ ! -f "${wp_dst}" ] && cp -ZPva "${wp_src}" "${wp_dst}" >>"${STDOUT}" 2>>"${STDERR}"
    done
    echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Copying wallpapers."
fi
# }}}

# PipEnv # {{{
# --------------------
if [ $ADD_PIPENV_ENV == 'yes' ]; then
    hl_msg='Installing Pipenv.'
    echo_hl_msg='no'
    SWAY_CONFIG_PATH="${UHOME}/.config/sway"
    echo -e "\t${DONEEMPTY} ${hl_msg}"

    MYPKGS=(python3-pip)
    missing=$(get_missing_rpms "${MYPKGS[@]}")
    if [ ! -z "${missing}" ]; then
        echo -e "\t${PREVLINE}${CLRLINE}${FAILFILLED} ${hl_msg} '${RED}python3-pip${NC}${FG}' is missing!"

        echo -e "\t${DONEEMPTY} Installing PIP."
        dnf -y install ${missing} >>${STDOUT} 2>>${STDERR}
        # Building DBs (3/3)
        rpm -qa --queryformat='%{NAME}\n' > ${INRPMS} 2>>${STDERR}
        do_as_user_noredirect "pip list --format=freeze | cut -d'=' -f1 > '${INPIPS}' 2>>'${STDERR}'"
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Installing PIP."
        hl_msg='Retry installing Pipenv.'
        echo_hl_msg='yes'
    fi

    [ $echo_hl_msg == 'yes' ] && echo -e "\t${DONEEMPTY} ${hl_msg}"
    if ! grep '^pipenv$' ${INPIPS} >/dev/null 2>&1; then
        do_as_user_noredirect "pip install --user pipenv >>'${STDOUT}' 2>>'${STDERR}'"
        do_as_user_noredirect "pip list --format=freeze | cut -d'=' -f1 > '${INPIPS}' 2>>'${STDERR}'"
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} ${hl_msg}"
    else
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} ${hl_msg} (Already was installed)"
    fi

    echo -e "\t${DONEEMPTY} Creating Pipenv environment."
    if ! su - ${USERNAME} -c "cd ${SWAY_CONFIG_PATH}/bin >/dev/null 2>&1; pipenv --venv >/dev/null 2>&1" >/dev/null 2>&1; then
        do_as_user_noredirect "cd '${SWAY_CONFIG_PATH}/bin' >>'${STDOUT}' 2>>'${STDERR}'; \
            pipenv --python /usr/bin/python >>'${STDOUT}' 2>>'${STDERR}'; \
            pipenv install -r requirements-pipenv.txt >>'${STDOUT}' 2>>'${STDERR}'"
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Creating Pipenv environment."
    else
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Creating Pipenv environment. (Already was created)"
    fi
fi
# }}}

# Cloning & installing vimConfig # {{{
# --------------------
if [ $ADD_VIMCONFIG == 'yes' ]; then
    # Cloning vimConfig
    echo -e "\t${DONEEMPTY} Cloning vimConfig."
    VC="${UHOME}/.config/vimConfig"
    VIM="${UHOME}/.vim"
    VIMRC="${UHOME}/.vimrc"

    rename_if_file "${VC}"
    rename_if_link "${VC}"

    if [ ! -d "${VC}" ]; then
        # Cloning vimConfig
        do_as_user "cd '${UHOME}/.config'; git clone https://gitlab.com/DeaDSouL/vimConfig.git"
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Cloning vimConfig."
    else
        # if it is already cloned
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Cloning vimConfig. (Already was cloned)"
    fi

    # Sym-linking VIMs
    echo -e "\t${DONEEMPTY} Sym-linking '.vimrc'."
    if [[ -L "${VIMRC}" && $(readlink "${VIMRC}") == "${VC}/vimrc" ]]; then
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Sym-linking '.vimrc'. (Already was sym-linked)"
    else
        rename_if_exist "${VIMRC}"
        rename_if_link "${VIMRC}"
        do_as_user "ln -vs '${VC}/vimrc' '${VIMRC}'"
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Sym-linking '.vimrc'."
    fi

    echo -e "\t${DONEEMPTY} Sym-linking '.vim'."
    if [[ -L "${VIM}" && $(readlink "${VIM}") == "${VC}/vim" ]]; then
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Sym-linking '.vim'. (Already was sym-linked)"
    else
        rename_if_exist "${VIM}"
        rename_if_link "${VIM}"
        do_as_user "ln -vs '${VC}/vim' '${VIM}'"
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Sym-linking '.vim'."
    fi

    # Creating needed dirs
    echo -e "\t${DONEEMPTY} Creating vimConfig needed directories."
    rename_if_file "${VC}/vim/plugged"
    rename_if_link "${VC}/vim/plugged"
    if [[ ! -d "${VC}/vim/plugged" || ! -d "${VC}/vim/files/bkp" || ! -d "${VC}/vim/files/inc" || ! -d "${VC}/vim/files/swap" || ! -d "${VC}/vim/files/undo" ]]; then
        do_as_user "mkdir -Zvp '${VC}/vim/'{plugged,files/{bkp,inc,swap,undo}}"
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Creating vimConfig needed directories."
    else
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Creating vimConfig needed directories. (Already were created)"
    fi
fi
# }}}

# Cloning & installing Nerd-Fonts # {{{
# --------------------
if [ $ADD_NERD_FONTS == 'yes' ]; then
    echo -e "\t${DONEEMPTY} Cloning Nerd Fonts."
    NF="${UHOME}/nerd-fonts"
    NFPATH="${UHOME}/.local/share/fonts/NerdFonts"
    # Cloning Nerd-Fonts
    if [[ ! -d "${NFPATH}" || $(find "${NFPATH}" -type f 2>/dev/null | wc -l 2>/dev/null) -eq 0 ]]; then
        rename_if_exist "${NF}"
        rename_if_link "${NF}"
        do_as_user "cd '${UHOME}'; git clone --depth 1 https://github.com/ryanoasis/nerd-fonts.git"
        do_as_user "cd '${NF}' && ./install.sh"
        sleep 2
        [ -e ${NF} ] && rm -rvf ${NF} >>${STDOUT} 2>>${STDERR}
        if [ ! -e ${NF} ]; then
            [ -e "${NF}.bkp-${DTIME}" ] && mv -Zv "${NF}.bkp-${DTIME}" "${NF}" >>"${STDOUT}" 2>>"${STDERR}"
        fi
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Cloning Nerd Fonts."
    else
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Cloning Nerd Fonts. (Already was installed)"
    fi
fi
# }}}

# Cloning Noir-Wallpapers # {{{
# --------------------
if [ $ADD_NOIR_WALLPAPERS == 'yes' ]; then
    echo -e "\t${DONEEMPTY} Cloning Noir-Wallpapers."
    path_wps="${UHOME}/Pictures/wallpapers"
    dir_noir="noir-wallpapers"

    # if ~/Pictures/wallpapers is a file, rename it
    rename_if_file "${path_wps}"

    # if ~/Pictures/wallpapers doesn't exist, create it
    [ ! -d "${path_wps}" ] && mkdir -Zpv "${path_wps}" >>"${STDOUT}" 2>>"${STDERR}"

    if [[ ! -d "${path_wps}/${dir_noir}" || $(find "${path_wps}/${dir_noir}" -type f 2>/dev/null | wc -l 2>/dev/null) -eq 0 ]]; then
        rename_if_dir "${path_wps}/${dir_noir}"
        rename_if_link "${path_wps}/${dir_noir}"
        do_as_user "cd '${path_wps}'; git clone https://gitlab.com/by-others/noir-wallpapers.git"
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Cloning Noir-Wallpapers."
    else
        echo -e "\t${PREVLINE}${CLRLINE}${DONEFILLED} Cloning Noir-Wallpapers. (Already cloned)"
    fi
fi
# }}}

# Printing some notes # {{{
# --------------------
echo -e "\n${BWHITE}Notes:${NC}${FG}"
# vimConfig # {{{
if [[ $ADD_VIMCONFIG == 'yes' \
    && -d "${VC}" && -d "${VC}/vim/plugged" \
    && -d "${VC}/vim/files/bkp" && -d "${VC}/vim/files/inc" \
    && -d "${VC}/vim/files/swap" && -d "${VC}/vim/files/undo" \
    && -L "${VIMRC}" && -L "${VIM}" \
    && $(readlink "${VIMRC}") == "${VC}/vimrc" \
    && $(readlink "${VIM}") == "${VC}/vim" ]]; then
    echo -e "\t${BWHITE}vimConfig${NC}${FG} has been cloned successfully."
    echo -e "\t${FG}You may need to do the following:"
    echo -e "\t    ${FG}1. Run ${BLUE}vim${NC}${FG}"
    echo -e "\t    ${FG}2. Upgrade ${BWHITE}vim-plug${NC}${FG}, by hitting: ${GREEN},U${NC}${FG}"
    echo -e "\t    ${FG}3. Install/Update plugins, by hitting: ${GREEN},u${NC}${FG}"
    echo -e "\t    ${FG}4. Exit ${BLUE}vim${NC}${FG}, by hitting ${GREEN}:q${NC}${FG} then hit ${GREEN}Enter${NC}${FG}"
    echo -e "\t    ${FG}5. Run the following command:"
    echo -e "\t        ${BLUE}bash ${UHOME}/.vim/plugged/fonts/install.sh${NC}${FG}"
    echo ''
fi
# }}}
# Nerd-Fonts # {{{
if [[ $ADD_NERD_FONTS == 'yes' \
    && -d "${NFPATH}" \
    && $(find "${NFPATH}" -type f 2>/dev/null | wc -l 2>/dev/null) -gt 0 ]]; then
    echo -e "\t${BWHITE}Nerd-Fonts${NC}${FG} path: ${BLUE}${NFPATH}${NC}${FG}"
    echo ''
fi
# }}}
# KeyBindings # {{{
if [ $COPY_DOTFILES == 'yes' ]; then
    echo -e "\t${BWHITE}KeyBindings${NC}${FG} CheatSheet: ${GREEN}Meta${NC}${FG}+${GREEN}Shift${NC}${FG}+${GREEN}/${NC}${FG}"
    echo ''
fi
# }}}
echo -e "\t${FG}Enjoy ${RED};)${NC}${FG}\n"
# }}}



# Finalizing things # {{{
# Ensure to have the correct ownership
MODIFIED_PATHS=(
    "${UHOME}/wallpaper"
    "${UHOME}/Pictures/wallpapers"
    $(for _dotfiles in "${CPDIRS[@]}"; do echo "${UHOME}/.config/${_dotfiles}"; done)
    "${UHOME}/.local/share/plasma/desktoptheme"
    "${UHOME}/.themes"
    "${UHOME}/.icons"
    "${TMPDIR}"
)
for f in $(find "${MODIFIED_PATHS[@]}" -not -user "${USERNAME}" -or -not -group "${USERNAME}" 2>/dev/null); do
    chown -v "${USERNAME}:${USERNAME}" "${f}" >>"${STDOUT}" 2>>"${STDERR}"
done

# generate wallpapers cache
do_as_user "${UHOME}/.config/sway/bin/pick-wallpaper.sh init" &

# start random wallpaper daemon
do_as_user "${UHOME}/.config/sway/bin/random_bg.sh start" &
# }}}


exit 0
