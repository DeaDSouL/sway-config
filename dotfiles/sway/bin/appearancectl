#!/bin/bash
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4 foldmethod=marker
# ----------------------------------------------------------------------


# ----------------------------------------------------------------------
# Variables # {{{
# GTK
gtk3_settings="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-3.0/settings.ini"
gnome_schema="org.gnome.desktop.interface"
declare -A gtk3         # (assoc-array) holds the gtk-3.0/settings.ini
declare -a gtk_themes   # (array)       holds the available gtk themes

# ICONS
declare -a icon_themes  # (array)       holds the available icon themes

# SDDM
declare -a sddm_themes  # (array)       holds the available SDDM themes
# }}}
# ----------------------------------------------------------------------
# Helpers {{{
# GTK # {{{
# ~/.config}/gtk-3.0/settings.ini
function _gtk3_settings_exists() { # {{{
    [ ! -f "$gtk3_settings" ] && return 1
    return 0
} # }}}
function _load_gtk3_settings() { # {{{
    local line current_section key value

    [ -z "$gtk3_settings" ] && return 1     # if $gtk3_settings is empty
    [ ! -f "$gtk3_settings" ] && return 2   # If $gtk3_settings does not exist
    [ ! -s "$gtk3_settings" ] && return 3   # If $gtk3_settings doesn't have data
    [ ! -r "$gtk3_settings" ] && return 4   # If $gtk3_settings is not readable

    declare -A ini_values=()

    while IFS= read -r line || [[ -n "$line" ]]; do
        # Skip empty lines and comments
        [[ "$line" =~ ^\; ]] && continue
        [[ "$line" =~ ^\# ]] && continue
        [[ -z "$line" ]] && continue

        # Parse sections
        if [[ "$line" =~ ^\[.*\]$ ]]; then
            current_section="${line#[}"
            current_section="${current_section%]}"
            continue
        fi

        # Parse key-value pairs
        key="${line%%=*}"
        value="${line#*=}"

        # Store the values in an associative array
        ini_values["$current_section.$key"]=$value
    done < "$gtk3_settings"

    # If the given ini file doesn't have data
    [ "${#ini_values[@]}" -eq 0 ] && { unset ini_values; return 3; }

    # Return the associative array
    echo "${ini_values[@]@K}"

    unset ini_values
    return 0
} # }}}
function _save_gtk3_settings() { # {{{
    local key value section found

    [ -z "$gtk3_settings" ] && return 1     # if $gtk3_settings is empty
    [ ! -f "$gtk3_settings" ] && return 2   # If $gtk3_settings does not exist
    [ ! -w "$gtk3_settings" ] && return 4   # If $gtk3_settings is not writable

    # Iterate over the associative array
    for key in "${!gtk3[@]}"; do
        value="${gtk3[$key]}"

        # Extract section and key from the key
        section="${key%%.*}"
        key="${key#*.}"

        # Check if the key exists under the section
        found=$(awk -F '=' -v k="$key" -v s="$section" '$1==k && section==s {print 1; found=1} END {if (!found) print 0}' section="$section" "$gtk3_settings")

        # If the key exists, update its value
        if [ "$found" -eq 1 ]; then
            sed -i "/^\[$section\]/,/^\[/ s/^\($key\s*=\s*\).*$/\1$value/" "$gtk3_settings"
        else
            # If the section doesn't exist in the file, add it
            grep -q "\[$section\]" "$gtk3_settings" || echo "[$section]" >> "$gtk3_settings"
            # Write the key-value pair to the file
            echo "$key=$value" >> "$gtk3_settings"
        fi
    done

    return 0
} # }}}
# themes
function _get_gtk_themes() { # {{{
    mapfile -t gtk_themes < <(find ~/.themes /usr/share/themes -maxdepth 1 -mindepth 1 -type d -exec basename '{}' -print \; 2>/dev/null)
} # }}}
# gsettings
function _get_gschema_key_value() { # {{{
    # 1: gnome-schema-key
    local ret value1 key="$1"

    # if $key is empty
    [ -z "$key" ] && {
        echo "Missing gnome-schema-key";
        return 2;
    }

    # force redirecting the output to stdout, so that we can capture it in variable
    value=$(gsettings get "${gnome_schema}" "${key}" 1>/dev/stdout 2>&1)
    ret=$?

    [ $ret -ne 0 ] && echo "Invalid key: '${key}'";
    [ $ret -eq 0 ] && echo "${value}"

    return ${ret:-1}
} # }}}
function _set_gschema_key_value() { # {{{
    # 1: gnome-schema-key
    # 2: gnome-schema-value
    local key="$1" value="$2"

    [ -z "$key"  ] && return 2  # if $key is empty
    [ -z "$value" ] && return 3 # If $key is empty
    [ ! _get_gschema_key_value "${key}" 1>/dev/null 2>&1 ] && return 4    # if $key is invalid

    if gsettings set "${gnome_schema}" "${key}" "${value}"; then
        return 0
    else
        return 1
    fi
} # }}}
# }}}
# QT (KDE Plasma) # {{{
# }}}
# ICONS # {{{
# themes
function _get_icon_themes() { # {{{
    mapfile -t icon_themes < <(find ~/.icons /usr/share/icons -maxdepth 1 -mindepth 1 -type d -exec basename '{}' -print \; 2>/dev/null)
} # }}}
# }}}
# SDDM #{{{
# themes
function _get_sddm_themes() { # {{{
    mapfile -t sddm_themes < <(find /usr/share/sddm/themes -maxdepth 1 -mindepth 1 -type d -exec basename '{}' -print \; 2>/dev/null)
} # }}}
# }}}
# }}}
# ----------------------------------------------------------------------
# Actions # {{{
# GTK # {{{
function get_gtk_themes() { # {{{
    local theme prefix \
        applied_gtk_theme=$(_get_gschema_key_value gtk-theme)
    _get_gtk_themes

    [ "${#gtk_themes[@]}" -eq 0 ] && return 1

    for theme in "${gtk_themes[@]}"; do
        prefix='[ ]'
        [ "$theme" == "${applied_gtk_theme:1:-1}" ] && prefix='[!]'
        [ "$theme" == "${gtk3[Settings.gtk-theme-name]}" ] && prefix='[✓]'
        echo " ${prefix} ${theme}"
    done

    return 0
} # }}}
function set_gtk_theme() { # {{{
    local is_valid_theme=1

    # load available gtk-themes
    _get_gtk_themes

    # make sure the given name is an existed theme
    for theme in "${gtk_themes[@]}"; do
        [ "$theme" == "$1" ] && {
            is_valid_theme=0;
            break;
        }
    done

    # if the given theme isn't found, exit
    [ "$is_valid_theme" -ne 0 ] && {
        echo "There is no GTK theme named: '$1'!";
        echo "To list available GTK themes, try: 'appearancectl get gtk-themes'!";
        return 2;
    }

    # modify the current gtk3 settings
    gtk3[Settings.gtk-theme-name]="$1"

    # save modifications
    if _save_gtk3_settings; then
        echo "GTK theme-name has been successfully changed to '$1'!"
        return 0
    else
        echo "Could not change GTK theme-name to '$1'!"
    fi

    return 1
} # }}}
function apply_gtk_theme() { # {{{
    local ret

    if [[ $(_gtk3_settings_exists; echo $?) -eq 0 && "${#gtk3[@]}" -gt 0 ]]; then
        _set_gschema_key_value gtk-theme "${gtk3[Settings.gtk-theme-name]}"
        _set_gschema_key_value icon-theme "${gtk3[Settings.gtk-icon-theme-name]}"
        _set_gschema_key_value cursor-theme "${gtk3[Settings.gtk-cursor-theme-name]}"
        _set_gschema_key_value font-name "${gtk3[Settings.gtk-font-name]}"
        ret=$?

        echo "GTK theme has been applied successfully!"
    else
        echo 'There is no settings.ini to read from!'
        echo "The '$HOME/.config}/gtk-3.0/settings.ini' is missing!"
        ret=1
    fi

    return ${ret:-0}
} # }}}
function gtk3_info() { # {{{
    echo -e "Reading from: ${gtk3_settings}"
    for key in "${!gtk3[@]}"; do
        echo "  ${key} = ${gtk3[$key]}"
    done
    return 0
} # }}}
# }}}
# QT (KDE Plasma) # {{{
function get_qt_themes() { # {{{
    # @TODO: code it
    return 1
} # }}}
function set_qt_theme() { # {{{
    # @TODO: code it
    return 1
} # }}}
function apply_qt_theme() { # {{{
    # @TODO: code it
    return 1
} # }}}
# }}}
# ICONS # {{{
function get_icon_themes() { # {{{
    local theme prefix \
        applied_icon_theme=$(_get_gschema_key_value icon-theme)
    _get_icon_themes

    [ "${#icon_themes[@]}" -eq 0 ] && return 1

    for theme in "${icon_themes[@]}"; do
        prefix='[ ]'
        [ "$theme" == "${applied_icon_theme:1:-1}" ] && prefix='[!]'
        [ "$theme" == "${gtk3[Settings.gtk-icon-theme-name]}" ] && prefix='[✓]'
        echo " ${prefix} ${theme}"
    done

    return 0
} # }}}
function set_icon_theme() { # {{{
    local is_valid_theme=1

    # load available icon-themes
    _get_icon_themes

    # make sure the given name is an existed theme
    for theme in "${icon_themes[@]}"; do
        [ "$theme" == "$1" ] && {
            is_valid_theme=0;
            break;
        }
    done

    # if the given theme isn't found, exit
    [ "$is_valid_theme" -ne 0 ] && {
        echo "There is no icon theme named: '$1'!";
        echo "To list available icon themes, try: 'appearancectl get icon-themes'!";
        return 2;
    }

    # modify the current gtk3 settings
    gtk3[Settings.gtk-icon-theme-name]="$1"

    # save modifications
    if _save_gtk3_settings; then
        echo "Icon theme-name has been successfully changed to '$1'!"
        return 0
    else
        echo "Could not change icon theme-name to '$1'!"
    fi

    return 1
} # }}}
# }}}
# SDDM # {{{
function get_sddm_themes() { # {{{
    # @TODO: code it
    return 1
} # }}}
function set_sddm_theme() { # {{{
    # @TODO: code it
    return 1
} # }}}
# }}}
# }}}
# ----------------------------------------------------------------------
# MAYBE WILL GET DELETED.. # {{{
function apply_both_themes() { # {{{
    # @TODO: code it
    return 1
} # }}}
# }}}
# ----------------------------------------------------------------------
# MAIN # {{{
# MAIN - Checks # {{{
function check_issues_pre() { # {{{
    # @TODO: code it
    return 1
} # }}}
function check_issues_post() { # {{{
    local need_apply=1 \
        applied_gtk_theme=$(_get_gschema_key_value gtk-theme) \
        applied_icon_theme=$(_get_gschema_key_value icon-theme)

    # GTK
    [ "${gtk3[Settings.gtk-theme-name]}" != "${applied_gtk_theme:1:-1}" ] && {
        echo '----------------------------------------------------------------------';
        echo "The current selected gtk-theme is: ${gtk3[Settings.gtk-theme-name]}";
        echo "Yet, the current applied gtk-theme is: ${applied_gtk_theme:1:-1}";
        need_apply=0
    }

    # ICON
    [ "${gtk3[Settings.gtk-icon-theme-name]}" != "${applied_icon_theme:1:-1}" ] && {
        echo '----------------------------------------------------------------------';
        echo "The current selected icon-theme is: ${gtk3[Settings.gtk-icon-theme-name]}";
        echo "Yet, the current applied icon-theme is: ${applied_icon_theme:1:-1}";
        need_apply=0
    }

    # Need apply?
    [ $need_apply -eq 0 ] && {
        echo '----------------------------------------------------------------------';
        echo 'To resolve this, you need to run: "appearancectl apply gtk"';
    }
} # }}}
# }}}
# MAIN - Routes # {{{
function helpers() { # {{{
    local \
        q=${1:-'main'} \
        sq=${2:-'main'}
    case "$q" in
        main) # {{{
            echo 'help -> main (default)'
            ;; # }}}
        get) # {{{
            case "$sq" in
                main)           echo "help -> $q -> $sq (default)"  ;;
                gtk-themes)     echo "help -> $q -> $sq"            ;;
                qt-themes)      echo "help -> $q -> $sq"            ;;
                icon-themes)    echo "help -> $q -> $sq"            ;;
                sddm-themes)    echo "help -> $q -> $sq"            ;;
                *)              echo "help -> $q -> $sq (Invalid)"; return 1 ;;
            esac
            ;; # }}}
        set) # {{{
            case "$sq" in
                main)           echo "help -> $q -> $sq (default)"  ;;
                gtk-theme)      echo "help -> $q -> $sq"            ;;
                qt-theme)       echo "help -> $q -> $sq"            ;;
                sddm-theme)     echo "help -> $q -> $sq"            ;;
                icon-theme)     echo "help -> $q -> $sq"            ;;
                *)              echo "help -> $q -> $sq (Invalid)"; return 1 ;;
            esac
            ;; # }}}
        apply) # {{{
            case "$sq" in
                main)   echo "help -> $q -> $sq (default)"  ;;
                gtk)    echo "help -> $q -> $sq"            ;;
                qt)     echo "help -> $q -> $sq"            ;;
                *)      echo "help -> $q -> $sq (Invalid)"; return 1 ;;
            esac
            ;; # }}}
        info) # {{{
            case "$sq" in
                main)   echo "help -> $q -> $sq (default)"  ;;
                gtk3)   echo "help -> $q -> $sq"            ;;
                qt)     echo "help -> $q -> $sq"            ;;
                sddm)   echo "help -> $q -> $sq"            ;;
                *)      echo "help -> $q -> $sq (Invalid)"; return 1 ;;
            esac
            ;; # }}}
        *)
            echo "Invalid command: '$q'";
            echo "Try: 'appearancectl help'";
            return 2
            ;;
    esac
    return 0
} # }}}
function getters() { # {{{
    local err_msg=''
    case "$1" in
        help)           helpers get "${@:2}"; ret=$?        ;;
        gtk-themes)     get_gtk_themes "${@:2}"; ret=$?     ;;
        qt-themes)      get_qt_themes "${@:2}"; ret=$?      ;;
        sddm-themes)    get_sddm_themes "${@:2}"; ret=$?    ;;
        icon-themes)    get_icon_themes "${@:2}"; ret=$?    ;;
        *)
            [ -z "$1" ] && err_msg="Missing sub-command!"
            echo ${err_msg:-"Invalid sub-command: '$1'!"}
            echo "Try: 'appearancectl help get'";
            return 2
            ;;
    esac
    return ${ret:-0}
} # }}}
function setters() { # {{{
    local err_msg=''
    case "$1" in
        help)           helpers set "${@:2}"; ret=$?    ;;
        gtk-theme)      set_gtk_theme "${@:2}"; ret=$?  ;;
        qt-theme)       set_qt_theme "${@:2}"; ret=$?   ;;
        sddm-theme)     set_sddm_theme "${@:2}"; ret=$? ;;
        icon-theme)     set_icon_theme "${@:2}"; ret=$? ;;
        *)
            [ -z "$1" ] && err_msg="Missing sub-command!"
            echo ${err_msg:-"Invalid sub-command: '$1'!"}
            echo "Try: 'appearancectl help set'";
            return 2
            ;;
    esac
    return ${ret:-0}
} # }}}
function appliers() { # {{{
    local err_msg=''
    case "$1" in
        help)   helpers apply "${@:2}"; ret=$?  ;;
        gtk)    apply_gtk_theme; ret=$?         ;;
        qt)     apply_qt_theme; ret=$?          ;;
        *)
            [ -z "$1" ] && err_msg="Missing sub-command!"
            echo ${err_msg:-"Invalid sub-command: '$1'!"}
            echo "Try: 'appearancectl help apply'";
            return 2
            ;;
    esac
    return ${ret:-0}
} # }}}
function infos() { # {{{
    local err_msg=''
    case "$1" in
        help)   helpers get "${@:2}"; ret=$?    ;;
        gtk3)   gtk3_info "${@:2}"; ret=$?      ;;
        qt)     qt_info "${@:2}"; ret=$?        ;;
        sddm)   sddm_info "${@:2}"; ret=$?      ;;
        *)
            [ -z "$1" ] && err_msg="Missing sub-command!"
            echo ${err_msg:-"Invalid sub-command: '$1'!"}
            echo "Try: 'appearancectl help info'";
            return 2
            ;;
    esac
    return ${ret:-0}
} # }}}
function main() { # {{{
    local err_msg=''
    check_issues_pre
    declare -A gtk3="($(_load_gtk3_settings))"

    # Pass all arguments except the first one
    # "${@:2}"
    case "$1" in
        help)       helpers "${@:2}"; ret=$?    ;;
        get)        getters "${@:2}"; ret=$?    ;;
        set)        setters "${@:2}"; ret=$?    ;;
        apply|load) appliers "${@:2}"; ret=$?   ;;
        info|query) infos "${@:2}"; ret=$?      ;;
        *)
            [ -z "$1" ] && err_msg="Missing command!"
            echo ${err_msg:-"Invalid command: '$1'!"}
            echo "Try 'appearancectl help'"
            ret=2
            ;;
    esac

    [ "$1" != 'help' ] && check_issues_post

    exit ${ret:-0}
} # }}}
# }}}
# }}}
# ----------------------------------------------------------------------

main $@
