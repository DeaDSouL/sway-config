# Swayora
The *ora* in Sway*ora* came from Fed*ora*.


## Description
Swayora is a collection of various configurations. That will transform the default Sway theme into a stylish nice one. And thanks to the useful shell and python scripts that Swayora comes shipped with, will give you an extended nice capabilities.

It was written especially for [Fedora](https://fedoraproject.org/), since it relies on `RPM`, `DNF`, and the packages names in Fedora's and RPMFusion's repositories.


## Screenshots
Swayora:
![alt Swayora: Multiple windows][multiple_windows]


Waybar:

![alt Waybar: Fedora theme][waybar_theme_fedora]


Dunst Notifications:

![alt Dunst: Notification theme][dunst_notification]


## Features

**Swayora** is meant to -depends on user's preference- automate the following:
- **The installation of:**
  - [x] The required packages for the script and its binaries.
- **Optional installation of:**
  - [ ] The user's selected packages.
  - [ ] `RPMFusion` repository.
  - [ ] [`Flatpak`](https://www.flatpak.org/) package and [`Flathub`](https://flathub.org/) repository.
  - [ ] [`Brave Browser`](https://brave.com/).
  - [ ] [`Nerd Fonts`](https://github.com/ryanoasis/nerd-fonts.git).
  - [ ] [`vimConfig`](https://gitlab.com/DeaDSouL/vimConfig.git).
  - [ ] [`Noir Wallpapers`](https://gitlab.com/by-others/noir-wallpapers.git).
- **The tweaking of:**
  - [ ] `/etc/dnf/dnf.conf`. by setting `fastestmirror=True` and `max_parallel_downloads=10`. (Optional).
  - [ ] `/etc/sudoers`, for our `kbbl` script to work, only if `light` is missing.
- **The configuration of:**
  - [x] `Sway`.
    - [x] Has a custom binary scripts living in `~/.config/sway/bin`.
    - [x] The config file has been splitted into multiple files in `~/.config/sway/config.d`.
  - [x] `Swaylock`.
    - [x] Use the default wallpaper in `~/.config/wallpaper` as the lock-screen wallpaper.
  - [x] `Waybar`.
    - [x] Custom available Fedora (DNF) updates indicator. (left click to upgrade, right click to list them, to close it `Meta+Shift+Q`).
    - [x] Custom available Flatpak updates indicator. (to close it `Meta+Shift+Q`).
  - [x] `Rofi`.
    - [x] Stylish quick launcher with `Meta+Space`.
    - [x] Fancy Gnome-Activities launcher with `Meta+Shift+Space`.
  - [x] `imv`.
    - [x] Set the current picture as a `Sway` wallpaper by hitting `w`.
  - [x] `zathura`.
  - [x] `Thunar`.
    - [x] Replace the default XFCE Terminal launcher custom action in the `Thunar` context menu with `foot`, to be able to launch terminals in current or the selected directory.
    - [x] Set the selected image as a `Sway` wallpaper via `Thunar` context menu.


## Installation
1. First, clone the repository.

```
git clone https://gitlab.com/DeaDSouL/swayora.git
```

2. Edit `swayora/install/config.sh` to meet your needs.

3. Run the installer as a normal user:

```
bash swayora/install/install.sh
```

4. If your keyboard has lights, you may want to modify the value (`/sys/class/leds/smc::kbd_backlight/brightness`) of the variable `BACKLIGHT` in `~/.config/sway/bin/kbbl` to meet the actual path to brightness for your keyboard.


## Keybindings
You can access the keybindings cheatsheet by hitting `Meta+Shift+/` and to exit hit `q`.

```
Sway:
    Meta+Shift+c        Reload Sway config
    Meta+Ctrl+l         Lock screen
    Meta+Shift+e        Exit Sway (Logout)
    Meta+Escape         Toggle passthrough mode

Waybar:
    Meta+Shift+w        Reload Waybar config
    Meta+Shift+b        Toggle visibility of Waybar

Bins:
    Meta+y              Browse the YouTube from terminal
    Alt+1               Enable inactive windows transparency (enabled by default,
                        see sway/config.d/95-autostart.conf)
    Alt+Shift+1         Disable inactive windows transparency

    Alt+2               Select a wallpaper
    Alt+Shift+2         Browse wallpapers and set them by hitting 'w'
    Meta+Shift+r        Change wallpaper to a random one

Basic:
    Meta+Return         Terminal (foot)
    Meta+`              File manager (Thunar)
    Meta+Shift+q        Kill focused window

Launchers:
    Meta+Space          Rofi Launcher (Alias: Meta+d)
    Meta+Shift+Space    Fancy Launcher for GUI applications (another Rofi theme)

Screenshot:
    Meta+VolumeUp       Capture the currently active output
    Meta+VolumeDown     Capture the currently active window
    Meta+VolumeMute     Select & capture a custom rectangular area

Tiling Modes:
    Meta+s              Switch to Stacking layout
    Meta+w              Switch to Tabbed layout
    Meta+e              Switch to split layout
    Meta+f              Switch to fullscreen layout

Layout stuff:
    Meta+b              Split current focused object horizontally
    Meta+v              Split current focused object vertically

Floating mode:
    Meta+Shift+f        Toggle floating to the focused window
    Alt+Space           Swap focus between the tiling area and floating area
    Meta+left_button    Drag and move window
    Meta+right_button   Resize window

Workspaces:
    Meta+1..0           Switch to Workspace 1..10
    Meta+Shift+1..0     Move focused window to Workspace 1..10

Moving around:
    Meta+Shift+a        Move your focus to the parent container
    Meta+               Move your focus to
        l,j,k,h             left, down, up, right
    Meta+Shift+         Move the focused window to
        l,j,k,h             left, down, up, right

Scratchpad:
    Meta+Shift+-        Move the currently focused windo to the scratchpad
    Meta+-              Show the next scratchpad window or hide the focused scratchpad window

Resize:
    Meta+r              Activate resizing mode
    l                   Shrink width by 10px
    j                   Grow height by 10px
    k                   Shrink height by 10px
    h                   Grow width by 10px
```

Keep in mind that the **Screenshot** keybindgs were made for my MacBookAir mid-2013. Feel free to modify them.


## Authors and acknowledgment
So far it's just me, @DeaDSouL.


## Contributing
Any contribution is more than welcomed.


## Contact me
- Mastodon: @DeaDSouL@fosstodon.org
- Matrix: @deadsoul:fedora.im


## License
[GNU GPL-v3](https://gitlab.com/DeaDSouL/swayora/-/blob/main/LICENSE?ref_type=heads)


[multiple_windows]: img/multiple-windows.png "Swayora: Multiple windows"
[waybar_theme_fedora]: img/swayora-waybar-theme-fedora.png "Waybar: Fedora theme"
[dunst_notification]: img/dunst-notification.png "Dunst: Notification theme"
